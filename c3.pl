#! perl -w
#use strict;
use Win32::GUI ();
use Win32::API ();
use utf8;    # enable Perl to parse UTF-8 from this file
use Encode;
use Devel::Peek;
use Data::Dumper;
 
# t2w - convert arbitrary string (may be unicode or not) to UTF-16LE
# encoding. Windows uses UTF-16LE as its native Unicode encoding (the
# "W" version of functions).
sub t2w {
    my ($text) = @_;
    return encode( "UTF-16LE", $text . "\x00" );
}
 
our $SetWindowTextW_fn = undef;
 
sub text {
    my ( $control, $text ) = @_;
    if ( not $SetWindowTextW_fn ) {
        $SetWindowTextW_fn =
          Win32::API->new( "user32", "SetWindowTextW", "NP", "N" );
        die unless $SetWindowTextW_fn;
    }
    $SetWindowTextW_fn->Call( $control->{-handle}, t2w($text) );
}
 
my $string = 'Русский текст';
#print $string;    # this should warn about "Wide character in print",
    # confirming that $string was correctly parsed from UTF-8 into Perl's
    # internal representation
#print Dump($string);
 
my $main = Win32::GUI::Window->new(
    -name => 'Main',
    -size => [ 400, 300 ],
);
 
# The Label will inherit $win_main's font
my $label = $main->AddLabel( -text => 'this is a temp string', );
text( $label, $string );


my $lv1 = $main->AddListView(
#	-class => "RichEdit20A",
	-left   => 25,
	-top    => 50,
	-width  => 300,
	-height => 100,
	-checkboxes => 0,
	-singlesel => 1,
	-name => 'lv1',
	#-addstyle     => WS_CHILD | WS_VISIBLE | 1 | WS_VSCROLL,
	-fullrowselect => 1,
	-gridlines => 1,

);

$main->lv1->InsertColumn(
    -index   => 0, 
    -subitem => 1, 
    -width   => $main->lv1->ScaleWidth/2, 
    -text    => "Meals Sold инфо",
);


#$main->lv1->InsertColumn(
#    -index   => 1, 
#    -subitem => 1, 
#    -width   => $main->lv1->ScaleWidth/2, 
#    -text    => "Столбец 2",
#);


#$lv1->SetUnicodeFormat(1);

#print "GetUnicodeFormat:",$lv1->GetUnicodeFormat(),"\n";



$lv1->InsertItem (

	-selected => 0,
	-text => "String 1",
);

$lv1->InsertItem (

	-selected => 0,
	-text => "plain english - Строка 3",	
);

$lv1->InsertItem (

	-selected => 0,
	-text => "plain english - Строка 4",	
);

$lv1->InsertItem (
	-selected => 0,
	-text => "plain english - Строка 5",	
);


$lv1->InsertItem (
	-text => "plain english - Строка 7",	
);


my $res = $lv1->FindItem (
-1,
-string => "plain english - Строка 7"
);

print Dumper($res);

$lv1->Add(
{-text=>"tttttt"}
);


#$lv1->SetItem(
#	-index=>0,
#	-text=>"New text"
#);

#print Dumper($res);


#my $t = $lv1->GetItemText(0);
#print "item text:".$t . "\n";


#my @res = $lv1->GetColumn(0);
#print "Column info: " . Dump($res[1]);

#print "Items count is:" . $lv1->GetItemCount() . "\n";
#my $itemText = $lv1->GetItemText(1);
#print "Item text is:" . $itemText. "\n";




 
$main->Show;
Win32::GUI::Dialog();
