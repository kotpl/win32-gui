use UTF8;
use Win32::GUI;
use strict;
use warnings;
use Encode;
use Devel::Peek;
use Data::Dumper;

$|=1;
 
my $screen_width = Win32::GUI::GetSystemMetrics(0);   ## gets current screen width
my $screen_height = Win32::GUI::GetSystemMetrics(1);   ## gets screen height 
my $minwidth = 600;   ## width I want the window to start at
my $minheight = 240;   ## height I want the window to start at
my $DataWindow = new Win32::GUI::Window(
 -name   => "DataWindow",
 -top    => ($screen_height - $minheight)/2,   ## center window vertically 
 -left   => ($screen_width - $minwidth)/2,   ## center window horizontally
 -width  => $minwidth,
 -height => $minheight,
 -title  => "Fruit Chooser",
);
 
$DataWindow->AddLabel(
 -name   => "Combo_Label",
 -text   => "Fruit: ",
 -top    => 55,
 -left   => 25,
 -visible=> 1,
);
 
#my $FruitList = $DataWindow->AddComboboxEx(
my $FruitList = $DataWindow->AddCombobox(
 -name   => "Dropdown",
 -top    => 52,
 -left   => 90,
 -width  => 120,
 -height => 110,
 -tabstop=> 1,
# -style  => WS_VISIBLE | 3 | WS_VSCROLL | WS_TABSTOP,
);

 
my $itemValue = Encode::decode("utf8","тестовое значение");
#$FruitList->InsertItem("-text"=>$itemValue,"-item"=>0);
$FruitList->InsertItem($itemValue);
#$FruitList->InsertItem("-text"=>"apple","-item"=>1);
$FruitList->InsertItem("banana");
$FruitList->InsertItem("cantaloupe");
$FruitList->InsertItem("grapes");
$FruitList->InsertItem("orange");
$FruitList->InsertItem("pear");
$FruitList->InsertItem("star fruit");

#	my @tmp = $FruitList->GetItem(0);
#	print Dumper(\@tmp);
#	print "here is:" . $tmp[1] ."\n";





$DataWindow->Show();
Win32::GUI::Dialog();

 
sub Dropdown_Change {
   # my $text= $FruitList->Text($FruitList->GetString($FruitList->SelectedItem));

	my $text = 	($FruitList->GetString($FruitList->SelectedItem));
	#print Dump($text);
	print $text . "\n";
	#print Dumper($text);

#	print Dump($$text);
#	print Dumper($text);
}