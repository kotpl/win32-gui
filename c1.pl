use strict;
use warnings;
use Data::Dumper;
use Win32::GUI;

my $font = Win32::GUI::Font->new();

my @res =    $font->Info();
print Dumper(\@res);